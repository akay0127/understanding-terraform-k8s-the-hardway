###########################################
#create vpc
###########################################


# Create a VPC
resource "aws_vpc" "example" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "Akeem-vpc-${terraform.workspace}"
  }
}

# ##################################
# creating internet gateway
#####################################

resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id

  tags = {
    Name = "akeem-igw-${terraform.workspace}"
  }
}
# ##################################
# creating public subnet
#####################################
resource "aws_subnet" "public" {
  count = length(var.public_subnet_cidr)

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = local.a_zone[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "public_subnet_${count.index + 1}"
  }
}
# ##################################
# creating private subnet
#####################################
resource "aws_subnet" "private" {
  count             = length(var.private_subnet_cidr)
  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_cidr[count.index]
  availability_zone = local.a_zone[count.index]
  tags = {
    Name = "private_subnet_${count.index + 1}"
  }
}
# ##################################
# creating database subnet
#####################################
resource "aws_subnet" "database" {
  count             = length(var.database_subnet_cidr)
  vpc_id            = local.vpc_id
  cidr_block        = var.database_subnet_cidr[count.index]
  availability_zone = local.a_zone[count.index]
  tags = {
    Name = "database_subnet_${count.index + 1}"
  }
}
# ##################################
# creating public route table
#####################################
resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }


  tags = {
    Name = "public_route_table"
  }
}
# ##########################################
# creating public route table association
############################################


resource "aws_route_table_association" "public_subnet" {
  count = length(aws_subnet.public)

  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

# resource "aws_route_table_association" "public_subnet_2" {
#   subnet_id      = aws_subnet.public.id[1]
#   route_table_id = aws_route_table.public_route_table.id
# }

# ##########################################
#     creating default route table 
############################################

resource "aws_default_route_table" "example" {
  default_route_table_id = aws_vpc.example.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw.id
  }

  tags = {
    Name = "example"
  }
}
# ##########################################
#     creating nat gateway
############################################

resource "aws_nat_gateway" "gw" {
  depends_on = [aws_internet_gateway.gw]

  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = "gw NAT"

  }
}
resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.gw]
  vpc        = true
}
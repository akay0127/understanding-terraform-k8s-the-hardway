####################################################
# Creating Ec2-instance in Public subnet
####################################################

resource "aws_instance" "app1" {
  ami                  = data.aws_ami.ami.id
  subnet_id            = aws_subnet.private[0].id
  instance_type        = var.public_instance_type1
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = file("${path.module}/templates/app1.sh")
  # key_name        = "fareed"
  security_groups = [aws_security_group.static_sg.id]

  tags = {
    Name = "App1-${terraform.workspace}"
  }
}

resource "aws_instance" "app2" {
  ami                  = data.aws_ami.ami.id
  subnet_id            = aws_subnet.private[1].id
  instance_type        = var.public_instance_type
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = file("${path.module}/templates/app2.sh")
  # key_name        = "fareed"
  security_groups = [aws_security_group.static_sg.id]

  tags = {
    Name = "App2-${terraform.workspace}"
  }
}

resource "aws_instance" "registration_app" {
  depends_on = [ aws_db_instance.registration_app_db ]
 
  count = 2
  
  ami                  = data.aws_ami.ami.id
  subnet_id            = aws_subnet.private[1].id
  instance_type        = var.registration_app_instance_type
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = templatefile("${path.module}/templates/registration-app.tmpl",
  
    {
      application_version = "v1.2.01"
      hostname            = "${aws_db_instance.registration_app_db.address}"
      port                = "${var.port}"
      db_name             = "${var.db_name}"
      db_username         = "${var.db_username}"
      db_password         = "${random_password.password.result}"
    }
  
  )
  
  # key_name        = "fareed"
  security_groups = [aws_security_group.registration_App.id]

  tags = {
    Name = "registration_App-${terraform.workspace}"
  }
}
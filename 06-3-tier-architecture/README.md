building a highly 3-tier application on AWS using terraform end-to-end                  
# procedure
create a repo
configure the backend
create files
create workspaces
...
terraform init
terraform workspace new sbx
terraform workspace list

terraform plan/apply/destroy

terraform plan -var-file sbx.tfvars


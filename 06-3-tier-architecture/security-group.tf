
########################################################
# Creating Public /Instance SG
########################################################

resource "aws_security_group" "alb_sg" {
  name        = "alb_sg-${terraform.workspace}"
  description = "Allow traffic on port 80"
  vpc_id      = local.vpc_id

  ingress {
    description = "allow traffic on port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow traffic on port 443"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-sg-${terraform.workspace}"
  }
}

########################################################
# Creating Private Instance SG
########################################################

resource "aws_security_group" "static_sg" {
  name        = "static_sg-${terraform.workspace}"
  description = "Allow inbound from alb sg id"
  vpc_id      = local.vpc_id

  #   ingress {
  #     description      = "allow ingress traffic on port 22"
  #     from_port        = 22
  #     to_port          = 22
  #     protocol         = "tcp"
  #     cidr_blocks      = ["24.148.43.188/32"]
  #   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "static-sg-${terraform.workspace}"
  }
}

########################################################
# Creating Private Instance SG Rule
########################################################
resource "aws_security_group_rule" "Allow_ssh_from_public_instance" {
  security_group_id        = aws_security_group.static_sg.id
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id

}
########################################################
# Creating Registration APP SG
########################################################

resource "aws_security_group" "registration_App" {
  name        = "Registration_App_Sg-${terraform.workspace}"
  description = "Allow inbound from alb sg id"
  vpc_id      = local.vpc_id

  #   ingress {
  #     description      = "allow ingress traffic on port 22"
  #     from_port        = 22
  #     to_port          = 22
  #     protocol         = "tcp"
  #     cidr_blocks      = ["24.148.43.188/32"]
  #   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Registration_App_Sg-${terraform.workspace}"
  }
}

########################################################
# Creating Registration APP SG Rule
########################################################
resource "aws_security_group_rule" "Registration_security_groiup" {
  security_group_id        = aws_security_group.registration_App.id
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id

}

########################################################
# Creating Database SG
########################################################

resource "aws_security_group" "database_sg" {
  name        = "database_Sg-${terraform.workspace}"
  description = "Allow inbound from registartion app sg id"
  vpc_id      = local.vpc_id

  #   ingress {
  #     description      = "allow ingress traffic on port 22"
  #     from_port        = 22
  #     to_port          = 22
  #     protocol         = "tcp"
  #     cidr_blocks      = ["24.148.43.188/32"]
  #   }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "database_Sg-${terraform.workspace}"
  }
}

########################################################
# Creating Database SG Rule
########################################################
resource "aws_security_group_rule" "Databse_sg" {
  security_group_id        = aws_security_group.database_sg.id
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.registration_App.id

}


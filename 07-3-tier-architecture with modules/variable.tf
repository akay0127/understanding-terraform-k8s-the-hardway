
# variable "vpc_cidr" {
#   type        = string
#   description = "the value of vpc cidr"

# }

# variable "public_subnet_cidr" {
#   type        = list(any)
#   description = "value of public subnet cidr"
#   default     = ["10.0.2.0/24", "10.0.4.0/24"]

# }

# variable "private_subnet_cidr" {
#   type        = list(any)
#   description = "value of private subnet cidr"
#   default     = ["10.0.3.0/24", "10.0.5.0/24"]

# }

# variable "database_subnet_cidr" {
#   type        = list(any)
#   description = "value of database subnet cidr"
#   default     = ["10.0.55.0/24", "10.0.57.0/24"]

# }

variable "public_instance_type1" {
  type        = string
  description = "value for the instance type"
  default     = "t3.micro"

}

variable "public_instance_type" {
  type        = string
  description = "value for the instance type"
  default     = "t3.micro"

}

variable "registration_app_instance_type" {
  type        = string
  description = "value for the instance type"
  default     = "t3.micro"

}


variable "db_name" {
  type        = string
  description = "value for database name"
  default     = "webappdb"
}

variable "instance_class" {
  type        = string
  description = "(optional) describe your variable"
  default     = "db.t2.micro"
}

variable "db_username" {
  type        = string
  description = "(optional) describe your variable"
  default     = "kojitechs"
}

variable "port" {
  type        = number
  description = "database port"
  default     = 3306
}

# variable "dns_name" {
#   description = "This is the dns name of that would be used to connect to all applications"
#   type        = string
# }

# variable "subject_alternative_names" {
#   type = list(any)
# }


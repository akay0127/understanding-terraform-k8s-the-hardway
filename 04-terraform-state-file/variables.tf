
variable "bucket_name" {
    type = list
    description = "the name of the state bucket"
    default = [
                "akeem.state.bucket",
                "05.terraform.workspaces",
                "06.3.tier.architecture"
    ]
  
}


# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Akeem"
  }
}

# resource "aws_subnet" "public_subnets" {
#   count = length(var.public_subnet_cidr)
#   vpc_id     = local.vpc_id
#   cidr_block = var.public_subnet_cidr[count.index]
#   availability_zone = local.a_zone[count.index]
#   map_public_ip_on_launch = true
#   tags = {
#     Name = "public-subnets-${count.index + 1}"
#   }
# }

# resource "aws_subnet" "private_subnets" {
#   count = length(var.private_subnet_cidr)
#   vpc_id     = local.vpc_id
#   cidr_block = var.private_subnet_cidr[count.index]
#   availability_zone = local.a_zone[count.index]
#   tags = {
#     Name = "private-subnets-${count.index + 1}"
#   }
# }

# resource "aws_instance" "web" {
#   ami           = data.aws_ami.ami.id
#   instance_type = "t3.micro"
#   subnet_id = aws_subnet.public_subnets[0].id

#   tags = {
#     Name = "HelloWorld"
#   }
# }


# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "Akeem-${terraform.workspace}"
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_1_cidr
  availability_zone = local.a_zone[0]
  tags = {
    Name = "akeem-subnet_1-${terraform.workspace}"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_2_cidr
  availability_zone = local.a_zone[1]
  tags = {
    Name = "akeem-subnet_2-${terraform.workspace}"
  }
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro"

  tags = {
    Name = "HelloWorld-${terraform.workspace}"
  }
}
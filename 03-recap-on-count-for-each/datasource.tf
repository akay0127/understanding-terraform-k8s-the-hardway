
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_key_pair" "example" {
  key_name           = var.key_pair
  include_public_key = true

  # filter {
  #   name   = "tag:Component"
  #   values = ["web"]
  # }
}


data "aws_ami" "ami" {
    most_recent      = true
   owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }
     filter {
    name   = "root-device-type"
    values = ["ebs"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
}
}
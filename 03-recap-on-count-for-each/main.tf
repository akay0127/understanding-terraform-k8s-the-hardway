
###########################################
#create 2 ec2 instance
###########################################


# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Akeem"
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_1_cidr
  map_public_ip_on_launch = true
  availability_zone = local.a_zone[0]
  tags = {
    Name = "akeem-subnet_1"
  }
}

resource "aws_subnet" "subnet_2" {
  vpc_id     = local.vpc_id
  cidr_block = var.subnet_2_cidr
  availability_zone = local.a_zone[1]
  map_public_ip_on_launch = true
  tags = {
    Name = "akeem-subnet_2"
  }
}


resource "aws_instance" "web" {
    count = 2
  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro"
  key_name = data.aws_key_pair.example.key_name
  subnet_id = [aws_subnet.subnet_1.id,aws_subnet.subnet_2.id][count.index]
 
  tags = {
    Name = "app-${count.index + 1}"
  }
}
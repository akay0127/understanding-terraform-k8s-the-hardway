

variable "key_pair" {
    description = "pulling down keypair using data-source"
    type = string
    default = "fareed"
  
}


variable "subnet_1_cidr" {
    type = string   
    description = "value for subnet cidr"
    default = "10.0.1.0/24"
  
}

variable "subnet_2_cidr" {
    type = string   
    description = "value for subnet cidr"
    default = "10.0.2.0/24"
  
}


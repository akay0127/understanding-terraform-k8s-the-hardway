
################################
# CHILDE'S MODULE
################################

module "s3" {
    # source = "/Users/kojibello/Downloads/scr/understanding-terraform-k8-the-hardway/08-kojitechs-modules/s3-parent-module"
    source = "../"
    # relative path, absolute path
    # source = "git::https://gitlab.com/kojibello/understanding-terraform-k8-the-hardway.git//08-kojitechs-modules/s3-parent-module"

    bucket_name = "test.bucket.kojitechss"
    versioning = "Enabled"
}


locals {
  vpc_id =aws_vpc.example.id

  # a_zone = data.aws_availability_zones.available.names
}

###########################################
#create vpc
###########################################



# Create a VPC
resource "aws_vpc" "example" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.vpc_tags,
  )

}

# ##################################
# creating internet gateway
#####################################

resource "aws_internet_gateway" "gw" {
  vpc_id = local.vpc_id
tags = merge(
    { "Name" = var.name },
    var.tags,
    var.igw_tags,
  )

}
# ##################################
# creating public subnet
#####################################
resource "aws_subnet" "public" {
  count = length(var.public_subnet_cidr)

  vpc_id                  = local.vpc_id
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = element(var.a_zone,count.index)
  map_public_ip_on_launch = true
  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.public_subnet_tags,
  )

}
# ##################################
# creating private subnet
#####################################
resource "aws_subnet" "private" {
  count             = length(var.private_subnet_cidr)
  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet_cidr[count.index]
  availability_zone = element(var.a_zone,count.index)
  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.private_subnet_tags,
  )

}
# ##################################
# creating database subnet
#####################################
resource "aws_subnet" "database" {
  count             = length(var.database_subnet_cidr)
  vpc_id            = local.vpc_id
  cidr_block        = var.database_subnet_cidr[count.index]
  availability_zone = element(var.a_zone,count.index)
  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.database_subnet_tags,
  )

}
# ##################################
# creating public route table
#####################################
resource "aws_route_table" "public_route_table" {
  vpc_id = local.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }


  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.public_route_table_tags,
  )

}
# ##########################################
# creating public route table association
############################################


resource "aws_route_table_association" "public_subnet" {
  count = length(aws_subnet.public)

  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

# resource "aws_route_table_association" "public_subnet_2" {
#   subnet_id      = aws_subnet.public.id[1]
#   route_table_id = aws_route_table.public_route_table.id
# }

# ##########################################
#     creating default route table 
############################################

resource "aws_default_route_table" "example" {
  default_route_table_id = aws_vpc.example.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw.id
  }

   tags = merge(
    { "Name" = var.name },
    var.tags,
    var.default_route_table_tags,
  )

}
# ##########################################
#     creating nat gateway
############################################

resource "aws_nat_gateway" "gw" {
  depends_on = [aws_internet_gateway.gw]

  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public[0].id

  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.nat_gateway_tags,
  )

}
resource "aws_eip" "eip" {
  depends_on = [aws_internet_gateway.gw]
  vpc        = true
}

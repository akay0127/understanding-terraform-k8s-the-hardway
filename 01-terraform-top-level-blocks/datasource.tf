
# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}

# data.aws_availability_zones.available.names

data "aws_ami" "ami" {
    most_recent      = true
   owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }
     filter {
    name   = "root-device-type"
    values = ["ebs"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
}
}
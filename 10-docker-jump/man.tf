

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
    #   }

    #   filter {
    #     name   = "virtualization-type"
    #     values = ["hvm"]
    #   }
  }
}


module "networking" {

  source = "../08-kojitechs-modules/networking-modules"
  
  vpc_cidr = "10.0.0.0/16"
  a_zone = ["us-east-1a", "us-east-1b"]
  public_subnet_cidr = ["10.0.2.0/24", "10.0.4.0/24"]
  private_subnet_cidr = ["10.0.3.0/24", "10.0.5.0/24"]
  database_subnet_cidr = ["10.0.55.0/24", "10.0.57.0/24"]


}

# ################################################################################
# # CREATING SECURITY GROUP FOR BASTON SERVER.
# ################################################################################

resource "aws_security_group" "jump-server" {
  name        = "jump-server"
  description = "Allow traffic on port 80"
  vpc_id      = module.networking.vpc_id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jump-server"
  }
}

# resource "aws_key_pair" "this" {
#   key_name   = "jumpserver-key"
#   public_key = file("./templates/public-key")
# }


####################################################
# Creating Ec2-instance in Public subnet
####################################################

resource "aws_instance" "jump-server" {
  ami                  = data.aws_ami.ami.id
  subnet_id            = module.networking.public_subnet_id[0]
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
  user_data            = file("${path.module}/templates/jump-server.sh")
  key_name        = "fareed"
  vpc_security_group_ids = [aws_security_group.jump-server.id]

  tags = {
    Name = "jump-server-${terraform.workspace}"
  }
}

